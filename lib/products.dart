import 'package:flutter/material.dart';

class Products extends StatelessWidget {
  final List<Map<String, dynamic>> products;
  final Function deleteProduct;

  Products(this.products, {this.deleteProduct});

  Widget _buildProductItems(BuildContext context, int index) {
    return Card(
      child: Column(
        children: <Widget>[
          FittedBox(
            child: Image.asset(products[index]['imageURL']),
            fit: BoxFit.scaleDown,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                products[index]['title'],
                style: TextStyle(
                  fontSize: 22.0,
                  fontWeight: FontWeight.w500,
                ),
              ),
              SizedBox(
                width: 10.0,
              ),
              Container(
                decoration: BoxDecoration(color: Theme.of(context).accentColor),
                padding: EdgeInsets.symmetric(
                  horizontal: 5.0,
                  vertical: 2.0,
                ),
                child: Text(
                  '\$' + products[index]['price'].toString(),
                ),
              )
            ],
          ),
          DecoratedBox(
            child: Padding(
                padding: EdgeInsets.symmetric(vertical: 2.5, horizontal: 5.0),
                child: Text('Shahrake valiasr township, Tehran')),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(4.0),
                border: Border.all(
                  color: Colors.grey,
                  width: 1.0,
                )),
          ),
          SizedBox(
            height: 5.0,
          ),
          ButtonBar(
            alignment: MainAxisAlignment.center,
            children: <Widget>[
              FlatButton(
                color: Theme.of(context).primaryColorDark,
                child: Text("Detail"),
                onPressed: () => Navigator.pushNamed<bool>(
                        context, '/product/' + index.toString())
                    .then((bool returnedValue) {
                  if (returnedValue) {
                    deleteProduct(index);
                  }
                }),
              )
            ],
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return products.length > 0
        ? ListView.builder(
            itemBuilder: _buildProductItems,
            itemCount: products.length,
          )
        : Center(
            child: Text('No Product Found Yet, Please add some!'),
          );
  }
}
