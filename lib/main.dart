import 'package:flutter/material.dart';
import 'package:flutter_app/pages/auth.dart';
import 'package:flutter_app/pages/manage_products.dart';
import 'package:flutter_app/pages/product_detail.dart';
import 'package:flutter_app/product_control.dart';
import 'package:flutter_app/product_manager.dart';

// import 'package:flutter/rendering.dart';

void main() {
  // debugPaintSizeEnabled = true;
  // debugPaintBaselinesEnabled = true;
  // debugPaintPointersEnabled = true;
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _MyApp();
  }
}

class _MyApp extends State<MyApp> with TickerProviderStateMixin {
  List<Map<String, dynamic>> _products = [];

  //   @override
  // void initState() {
  //   if (widget.startingValue != null) {
  //     _products.add(widget.startingValue);
  //   }
  //   super.initState();
  // }

  // @override
  // void didUpdateWidget(ProductManager oldWidget) {
  //   print('didUpdateWidget');
  //   super.didUpdateWidget(oldWidget);
  // }

  void _setProduct(Map<String, dynamic> productName) {
    setState(() {
      _products.add(productName);
    });
  }

  void _deleteProduct(int index) {
    setState(() {
      _products.removeAt(index);
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
          brightness: Brightness.light,
          primarySwatch: Colors.deepOrange,
          accentColor: Colors.deepOrangeAccent,
          fontFamily: 'Oswald'),
      home: AuthenticatePage(_setProduct),
      routes: {
        '/manage/products': (BuildContext context) => ManageProducts(),
        '/ProductManager': (BuildContext context) =>
            ProductManager(_products, _setProduct, _deleteProduct),
      },
      onGenerateRoute: (RouteSettings setting) {
        final List<String> pathElements = setting.name.split("/");
        if (pathElements[0] != '') return null;
        if (pathElements[1] == 'product') {
          final int index = int.parse(pathElements[2]);
          return MaterialPageRoute<bool>(
            builder: (BuildContext context) => ProductDetail(
              title: _products[index]['title'],
              imageURL: _products[index]['imageURL'],
            ),
          );
        } else if (pathElements[1] == 'addProducts') {
          return MaterialPageRoute(
            builder: (BuildContext context) => ProductControl(_setProduct),
          );
        }

        return null;
      },
      onUnknownRoute: (RouteSettings setting) {
        return null;
      },
    );
    // body: ProductManager(startingValue: 'First Value')));
  }
}
