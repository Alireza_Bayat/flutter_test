import 'package:flutter/material.dart';

import 'package:flutter_app/Products.dart';
import 'package:flutter_app/product_control.dart';

class ProductManager extends StatelessWidget {
  final List<Map<String, dynamic>> products;
  final Function setProduct;
  final Function deleteProduct;

  ProductManager(this.products, this.setProduct, this.deleteProduct);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          margin: EdgeInsets.all(10.0),
          child: Card(
            child: ProductControl(setProduct),
          ),
        ),
        Expanded(
            child: Products(
          products,
          deleteProduct: deleteProduct,
        ))
      ],
    );
  }
}
