import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class ProductCreateNew extends StatefulWidget {
  final Function setProduct;

  const ProductCreateNew(this.setProduct);

  @override
  _ProductCreateNewState createState() => _ProductCreateNewState(setProduct);
}

class _ProductCreateNewState extends State<ProductCreateNew> {
  final Function setProduct;

  String _productName = '';
  String _productDescription = '';
  double _productPrice;
  bool _acceptTerms = false;

  _ProductCreateNewState(this.setProduct);
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(10.0),
      child: ListView(
        children: <Widget>[
          TextField(
            decoration: InputDecoration(
                labelText: 'Product Name', icon: Icon(Icons.title)),
            keyboardType: TextInputType.text,
            autocorrect: true,
            onChanged: (String value) {
              setState(() {
                _productName = value;
              });
            },
          ),
          TextField(
            decoration: InputDecoration(
                labelText: 'Description', icon: Icon(Icons.description)),
            keyboardType: TextInputType.text,
            minLines: 4,
            maxLines: 5,
            onChanged: (String value) {
              setState(() {
                _productDescription = value;
              });
            },
          ),
          TextField(
            decoration: InputDecoration(
                labelText: 'Price', icon: Icon(Icons.attach_money)),
            keyboardType: TextInputType.number,
            onChanged: (String value) {
              setState(() {
                _productPrice = double.parse(value.toString());
              });
            },
          ),
          SwitchListTile(
            title: Text('Accept Terms'),
            value: _acceptTerms,
            onChanged: (bool value) {
              setState(() {
                _acceptTerms = value;
              });
            },
          ),
          SizedBox(
            height: 10.0,
          ),
          RaisedButton(
            child: Text('save'),
            onPressed: () {
              String image =
                  _productPrice >= 2 ? 'assets/cam2.jpg' : 'assets/cam.jpg';
              Map<String, dynamic> details = {
                'price': _productPrice,
                'title': _productName,
                'imageURL': image
              };
              setProduct(details);
              Navigator.pushNamed(context, '/ProductManager');
            },
          )
        ],
      ),
    );
  }
}
