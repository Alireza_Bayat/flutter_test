import 'package:flutter/material.dart';

class ProductDetail extends StatelessWidget {
  final String title;
  final String imageURL;
  ProductDetail({this.imageURL, this.title});

  _showAlert(BuildContext context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Are you sure?'),
          content: Text('DELETING!'),
          actions: <Widget>[
            FlatButton(
              child: Text('discared'),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
            FlatButton(
              child: Text('continue'),
              onPressed: () {
                Navigator.pop(context);
                Navigator.pop(context, true);
              },
            )
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        Navigator.pop(context, false);
        return Future.value(false);
      },
      child: Scaffold(
          appBar: AppBar(
            title: Text(this.title),
          ),
          body: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Center(
                child: Image.asset(imageURL),
              ),
              Container(
                child: Text('No Detail Found Yet.' + '::>' + this.title),
                padding: EdgeInsets.all(10.0),
              ),
              Container(
                color: Theme.of(context).accentColor,
                child: FlatButton(
                    child: Text('DELETE Image'),
                    onPressed: () => _showAlert(context)),
              )
            ],
          )),
    );
  }
}
