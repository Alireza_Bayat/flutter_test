import 'package:flutter/material.dart';
import 'package:flutter_app/pages/products_admin.dart';

class AuthenticatePage extends StatelessWidget {
  final Function setProduct;
  const AuthenticatePage(this.setProduct);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          toolbarOpacity: 0.5,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.vertical(
              bottom: Radius.circular(300),
            ),
          ),
          bottom: PreferredSize(
            preferredSize: const Size.fromHeight(200.0),
            child: Theme(
              data: Theme.of(context).copyWith(accentColor: Colors.white),
              child: Container(
                height: 50.0,
                alignment: Alignment.center,
                child: (Text('data')),
              ),
            ),
          ),
          title: Text('Login Page'),
        ),
      body: Center(
        child: RaisedButton(
          child: Text('LOGIN'),
          onPressed: () {
            Navigator.pushReplacement(
                context,
                MaterialPageRoute(
                          builder: (BuildContext context) =>
                              ProductsPage(setProduct)));
                },
              ),
            ),
    );
  }
}
