import 'package:flutter/material.dart';

class ManageProducts extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Product Manager'),
      ),
      body: Center(
          child: RaisedButton(
        child: Text('Modal'),
        onPressed: () {
          showModalBottomSheet(
              context: context,
              builder: (BuildContext context) {
                return Center(
                  child: Text('Modal Detail'),
                );
              });
        },
      )),
    );
    ;
  }
}
