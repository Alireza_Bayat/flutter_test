import 'package:flutter/material.dart';
import 'package:flutter_app/pages/product_create.dart';
import 'package:flutter_app/pages/product_detail.dart';
import 'package:flutter_app/pages/product_list.dart';

class ProductsPage extends StatelessWidget {
  final Function setProduct;

  const ProductsPage(this.setProduct);

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        drawer: Drawer(
          child: Column(
            children: <Widget>[
              AppBar(
                automaticallyImplyLeading: false,
                title: Text('Choose Items'),
              ),
              ListTile(
                title: Text('Product Detail'),
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (BuildContext context) => ProductDetail(
                        title: 'From Drawer',
                        imageURL: 'assets/cam2.jpg',
                      ),
                    ),
                  );
                },
              ),
              ListTile(
                onTap: () {
                  Navigator.pushNamed(context, '/manage/products');
                },
                title: Text('Manage Products'),
              ),
              ListTile(
                onTap: () {
                  Navigator.pushNamed(context, '/ProductManager');
                },
                title: Text('Add Products'),
              ),
              ListTile(
                title: Text('Exit'),
                onTap: () {},
              ),
            ],
          ),
        ),
        appBar: AppBar(
          title: Text('Products Page'),
          centerTitle: true,
          bottom: TabBar(
            tabs: <Widget>[
              Tab(
                icon: Icon(Icons.create),
                text: 'Create',
              ),
              Tab(
                icon: Icon(Icons.list),
                text: 'View',
              )
            ],
          ),
        ),
        body: TabBarView(
          children: <Widget>[ProductCreateNew(setProduct), ProductList()],
        ),
      ),
    );
  }
}
